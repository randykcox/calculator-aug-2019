An HTML and CSS reproduction of the MacOS calculator, using flexbox layout. This was mob-coded by the SE August 2019 cohort during the demo on 09/30/2019.

[See the live page](https://randykcox.gitlab.io/calculator-aug-2019)